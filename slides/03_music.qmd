## Interim Summary

* The cochlear translates sound waves into envelopes at different frequencies
  * i.e. how loud is it at frequency X at time Y?
* The auditory nerve sends these envelopes to the brain
* We can see these envelopes in the brain data acquired with the MEG
* The strength of the reconstructed envelopes modulates with attention

![](assets/images/for_speech.svg){.fragment .absolute width="100%" top="0"}

## But are speech and music not similar?

* Highly structured but complex
* Auditory
* Part of culture &rarr; universal exposure

```{=html}
<figure style="object-fit: contain; height: 40%" class="center-elements fragment">
<img src="assets/images/abrams_speech_music_fmri.jpeg">
<figcaption>Abrams et al., 2011</figcaption>

</figure>
```

## Yes, but we find different selective areas
![Boebinger et al., 2021](assets/images/boebinger_selectivity.png)

## Yes, but we find different selective areas
![Norman-Haignere et al., 2015](assets/images/norman_fmri.jpg)

## Can we decode "music" from brain data?

```{=html}
<figure>
<div data-load="assets/images/sankaran_f1.svg" class="center-elements" width="200px" style="height: 90%"></div>
<figcaption class="freefloating">Sankaran et al., 2024</figcaption>
</figure>

```

## Yes, we can (when we open the skull...)
![Sankaran et al., 2024](assets/images/sankaran_f2.jpg)

## But also without opening the skull
![Zuk et al., 2021](assets/images/zuk.png)