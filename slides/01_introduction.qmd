## How does sound reach the brain?
```{=html}
<figure style="object-fit: contain; height: 90%" class="center-elements">
<img src="assets/images/cochlea_large.png" style="background: white; height: 70%">
<figcaption>https://philschatz.com/biology-book/contents/m44760.html</figcaption>

</figure>
```

## Tonotopy
```{=html}
<div class="in-row">
<figure style="width: 50%">
<img src="assets/images/cochlea.png" style="background: white">
<figcaption>https://philschatz.com/biology-book/contents/m44760.html</figcaption>
</figure>
<div style="width: 50%; height: 50%; object-fit: contain">
<div>
<figure class="fragment fade-out" data-fragment-index="1" style="position: absolute">
<img src="assets/images/tonotopy_simple.jpg">
<figcaption>https://www.ncbi.nlm.nih.gov/books/NBK10900/figure/A920/</figcaption>
</figure>
<figure class="fragment fade-in" data-fragment-index="1" style="position: absolute">
<img src="assets/images/tonotopy_complex.jpg">
<figcaption>Ellen et al., 2022</figcaption>
</figure>
</div>
</div>
</div>
```

## What frequencies can the brain "follow"?
![Kuwada et al., 2002](assets/images/freq_sensitivity.png){.center-elements}

## So...

* The auditory cortex needs to process sound as high as 20,000Hz
* But it can only follow as high as 100-300Hz
* What can we do?