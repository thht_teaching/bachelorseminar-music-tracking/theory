## Back to Speech for a bit
```{=html}
<figure style="height: 70%">
<div class="in-row" style="flex-wrap: wrap;">
<img src="assets/images/jin_2018_eeg.png">
<img src="assets/images/jin_2018_veog.png" class="half fragment" data-fragment-index="1">
<img src="assets/images/jin_2018_heog.png" class="half fragment" data-fragment-index="1">
</div>
<figcaption>Jin et al., 2018</figcaption>
</figure>
```

## And then we did this
```{=html}
<div style="display: flex; flex-direction: row; flex-wrap: wrap">
<img src="assets/images/man_reading.png" style="height: 200px; object-fit: cover; width: auto" class="fragment" data-fragment-index="1">
<p style="width: 80%" class="fragment" data-fragment-index="1">It was the middle of winter, and the snow-flakes were falling like feathers from the sky…</p>
<img src="assets/images/woman_reading.png" style="height: 200px" class="fragment" data-fragment-index="2">
<p style="width: 80%" class="fragment" data-fragment-index="2">…This gave the queen a great shock, and she became yellow and green with envy…</p>
</div>
```

## And then we did this
```{=html}
<div style="display: flex; align-items: center; gap: 20px; height: 600px">
<img src="assets/images/meg.png" style="width: 50%; object-fit: contain">
<img src="assets/images/eye_tracker.png" style="width: 50%; object-fit: contain">
</div>
```

## This is what we found
![](assets/images/results_eyetracking.png){.center}

## This is what we found
* Eye Tracking Data also track the Speech Envelope
* Ocular Tracking seems to be used when it gets hard

## So, extra question:
* Does the eye also track music?

## What are we going to do?
* Use the stimuli of Hausfeld
* Acquire MEG and Eyetracker
* Basically do a mTRF (Stimulus Reconstruction) in a 2x2 design:
  * Attended vs Unattended
  * MEG vs Eyetracker